<h1 align = "center"> :fast_forward: :snake: Snake Game :snake: :rewind: </h1>

## 🖥 Preview
<p align = "center">
  <img src = "https://scontent.fbnu2-1.fna.fbcdn.net/v/t1.0-9/117418819_1699567240197620_5480159582094259340_n.jpg?_nc_cat=102&_nc_sid=0debeb&_nc_eui2=AeGaAtllIqgDKAnSwE_XYJGVGvuxntMrhEYa-7Ge0yuERrI_VGGZoCxCeq5gWNY0VPwx2VtYIu1rQwT4ZLGNQRYq&_nc_ohc=k7GWDfArrBQAX98KWLM&_nc_ht=scontent.fbnu2-1.fna&oh=5c22273bd181a6809c42d7bec59e7a80&oe=5F57B3FD" width = "500">
</p>
<p align = "center">
  <img src = "https://scontent.fbnu2-1.fna.fbcdn.net/v/t1.0-9/117237899_1699567243530953_5660241378831014175_n.jpg?_nc_cat=108&_nc_sid=0debeb&_nc_eui2=AeHZKSLd4K743pzcxd8zeWfZWnoxsqLkJmZaejGyouQmZhXiTR1L5DSf61OUjVnZse2PKlB2kWgyk3xcELbJ_MG2&_nc_ohc=MnB3hwRDqpkAX-i11W3&_nc_ht=scontent.fbnu2-1.fna&oh=51cb70abf9eae811678a482070fa9821&oe=5F58F217" width = "500">
</p>
<p align = "center">
  <img src = "https://scontent.fbnu2-1.fna.fbcdn.net/v/t1.0-9/117791960_1699567273530950_1990442313873743727_n.jpg?_nc_cat=103&_nc_sid=0debeb&_nc_eui2=AeG14EKpb_5d053R0pe37BgObw7P1642LN1vDs_XrjYs3c5WfwK9JIJNBlZIX874K6AJCEB3yrXUA0kAu3Nw7AlL&_nc_ohc=BsvkFk6AHoEAX_EkwXt&_nc_ht=scontent.fbnu2-1.fna&oh=43a39452af94437f1b3e6177905ce922&oe=5F5A6DB8" width = "500">
</p>

---

## 📖 About
Simple snake game created with basic front-end elements.

---

## 🛠 Technologies used
- JavaScript
- CSS
- HTML

---

## 🚀 How to execute the project
#### Clone the repository
git clone https://github.com/EPieritz/snake_game.git

#### Enter directory
`cd snake_game`

#### Run the server
- right click on the `home.html` file
- click on `open with liveserver`

---
Developed with 💙 by Emilyn C. Pieritz
